package com.xman.shiro.chapter2;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.apache.shiro.util.ThreadContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class LoginLogoutTest {

    void authenticate(String fileInPath, String checkName, String checkPassword) {
        //1、获取SecurityManager工厂，此处使用Ini配置文件初始化SecurityManager
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:" + fileInPath);

        //2、得到SecurityManager实例 并绑定给SecurityUtils
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        //3、得到Subject及创建用户名/密码身份验证Token（即用户身份/凭证）
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(checkName, checkPassword);

        try {
            //4、登录，即身份验证
            subject.login(token);
        } catch (AuthenticationException e) {
            //5、身份验证失败
        }

        // 判断用户是否登陆
        Assert.assertEquals(true, subject.isAuthenticated());

        //6、退出
        subject.logout();
    }

    @Test
    public void testHelloWorld() {
        authenticate("shiro.ini", "zhang", "123");
    }

    @Test
    public void testCustomRealm() {
        authenticate("shiro-realm.ini", "zhang", "123");
    }

    @Test
    public void testCustomMultiRealm() {
        authenticate("shiro-multi-realm.ini", "zhang", "123");
    }

    @Test
    public void testJDBCRealm() {
        authenticate("shiro-jdbc-realm.ini", "zhang", "123");
    }

    @After
    public void tearDown() throws Exception {
        ThreadContext.unbindSubject();//退出时请解除绑定Subject到线程 否则对下次测试造成影响
    }

}
